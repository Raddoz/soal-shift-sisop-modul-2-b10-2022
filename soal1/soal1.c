#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>
#include <dirent.h>

void downloadAsset();
int countFiles(char* folder);
void storeFiles(FILE* asset[], char* folder);
void gacha(FILE* characters[], FILE* weapons[]);
void randomizer(FILE* asset[], char result[], int size);
void zipAll();

char basedir[50] = "/home/yeray/Sisop/modul_2/soal_1";

int main() {
    srand(time(0));
    pid_t pid, sid;
    
    pid = fork();

    if(pid < 0) {
        exit(EXIT_FAILURE);
    }
    if(pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if(sid < 0) {
        exit(EXIT_FAILURE);
    }

    if((chdir(basedir)) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    pid_t child_id_dw;
    int status_dw;
    child_id_dw = fork();

    if(child_id_dw == 0) {
        downloadAsset();
        // chdir("..");
    }
    else {
        while((wait(&status_dw)) > 0);
        pid_t child_id_dw2;
        int status_dw2;
        child_id_dw2 = fork();

        if(child_id_dw2 == 0) {
            char *argv[] = {"mkdir", "gacha_gacha", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while((wait(&status_dw2)) > 0);
            char folder[100];
            sprintf(folder, "characters");
            // FILE* characters[countFiles(folder)];
            FILE* characters[49];
            storeFiles(characters, folder);
            folder[0] = '\0';
            sprintf(folder, "weapons");
            // FILE* weapons[countFiles(folder)];
            FILE* weapons[131];
            storeFiles(weapons, folder);
            // gacha(characters, weapons);
            // system("pwd");
            // zipAll();
            
            while(1) {
                time_t t = time(NULL);
                struct tm tm = *localtime(&t);
                char currtime[50];

                pid_t child_id;
                int status;

                child_id = fork();

                if(child_id == 0) {
                    if(tm.tm_mon == 2 && tm.tm_mday == 30 && tm.tm_hour == 4 && tm.tm_min == 44) {
                        gacha(characters, weapons);
                    }
                    else if(tm.tm_mon == 2 && tm.tm_mday == 30 && tm.tm_hour == 7 && tm.tm_min == 44) {
                        zipAll();
                    }
                    else {
                        char *argv[] = {"echo", NULL};
                        execv("/usr/bin/echo", argv);
                    }
                }
                while((wait(&status)) > 0);
                if(tm.tm_mon == 2 && tm.tm_mday == 30 && tm.tm_hour == 7 && tm.tm_min == 44)
                    exit(EXIT_SUCCESS);
                sleep(1);
            }
        }
    }
    

}

void downloadAsset() {

    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id == 0) {
        char *argv[] = {"mkdir", "gacha_database", NULL};
        execv("/usr/bin/mkdir", argv);
    }
    else {
        while((wait(&status)) > 0);
        chdir("gacha_database");
        char *link[] = {
            "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
            "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
        };
        char *filename[] = {"characters.zip", "weapons.zip"};

        pid_t child_id2;
        int status2, i;

        for(i = 0; i < 2; i++) {
            if(child_id2 == fork()) {
                char *argv[] = {"wget", "-q", "--no-check-certificate", link[i], "-O", filename[i], NULL};
                execv("/usr/bin/wget", argv);
            }
            while((wait(&status2)) > 0);

            if(child_id2 == fork()) {
                char *argv[] = {"unzip", "-q", filename[i], NULL};
                execv("/usr/bin/unzip", argv);
            }
            while((wait(&status2)) > 0);

            // else {
            //     while((wait(&status2)) > 0);
            //     pid_t child_id3;
            //     int status3;

            //     child_id3 = fork();
            //     if(child_id3 == 0) {
            //         char *argv[] = {"unzip", "-q", filename[i], NULL};
            //         execv("/usr/bin/unzip", argv);
            //     }
            //     else
            //         while((wait(&status3)) > 0);
            // }
        }
        chdir("..");

    }

}

// int countFiles(char folder[]) {

//     DIR *d;
//     int n = 0;
//     struct dirent *dir;
//     d = opendir(folder);
//     if (d) {
//         while ((dir = readdir(d)) != NULL) {n++;}
//         closedir(d);
//     }
//     return n;
// }

void storeFiles(FILE* asset[], char folder[]) {

    chdir("gacha_database");
    // system("pwd");
    DIR *d;
    int i = 0;
    struct dirent *dir;
    d = opendir(folder);
    chdir(folder);
    if (d != NULL) {
        while ((dir = readdir(d)) != NULL) {
            asset[i] = fopen(dir->d_name, "r");
            // char temp[300];
            // sprintf(temp, "echo %s", dir->d_name);
            // system(temp);
            // FILE* tes = fopen(dir->d_name, "r");
            // char* buffer = malloc(sizeof(char) * 10000);;
            // fread(buffer, 10000, 1, tes);
            // fclose(tes);
            i++;
        }
        closedir(d);
        
        // char temp[50];
        // sprintf(temp, "echo masuk %d", i-1);
        // system(temp);
    }
    else {
        perror("storeFiles");
        exit(EXIT_FAILURE);
    }
    chdir("../..");
}

void gacha(FILE* characters[], FILE* weapons[]) {
    char gachafolder[100];
    sprintf(gachafolder, "%s/gacha_gacha", basedir);
    // chdir("/home/yeray/Sisop/modul_2/soal_1/gacha_gacha");
    chdir("gacha_gacha");
    // system("pwd");
    int primo, i = 0;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char currfilename[50];
    // sprintf(currfilename, "%02d:%02d:%02d_gacha_%d.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, 10);
    // FILE* currfile = fopen(currfilename, "w");
    FILE* currfile;
    for(primo = 79000; primo >= 160; primo -= 160) {
        if(i % 90 == 0) {
            int rem_gacha;
            if(primo >= 14400)
                rem_gacha = 90;
            else
                rem_gacha = primo/160;
            if(i != 0) chdir("..");
            char newFolder[20];
            sprintf(newFolder, "total_gacha_%d", i+rem_gacha);
            pid_t child_id;
            int status;
            if((child_id = fork()) == 0) {
                char *argv[] = {"mkdir", newFolder, NULL};
                execv("/usr/bin/mkdir", argv);
            }
            while((wait(&status)) > 0);
            chdir(newFolder);
            // system("pwd");
        }
        if(i % 10 == 0) {
            
            t = time(NULL);
            tm = *localtime(&t);
            int rem_gacha;
            if(primo >= 1600)
                rem_gacha = 10;
            else
                rem_gacha = primo/160;
            sprintf(currfilename, "%02d:%02d:%02d_gacha_%d.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, i+rem_gacha);
            if(i != 0) fclose(currfile);
            currfile = fopen(currfilename, "w");
            sleep(1);
        }
        // fprintf(currfile, "masuk %d\n", primo);
        char currgacha[50];
        char result[20];
        if(i+1 % 2 == 0) {
            randomizer(weapons, result, 130);
            sprintf(currgacha, "%d_weapons_%s_%d", i+1, result, primo-160);
        }
        else {
            randomizer(characters, result, 48);
            sprintf(currgacha, "%d_characters_%s_%d", i+1, result, primo-160);
        }
        fprintf(currfile, "%s\n", currgacha);
        i++;
    }
    fclose(currfile);
    chdir("../..");
    char *argv[] = {"echo", NULL};
    execv("/usr/bin/echo", argv);
}

void randomizer(FILE* asset[], char result[], int size) {
    // size_t size = sizeof(asset)/sizeof(FILE*);
    char buffer[10000];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

    FILE* randomFile = asset[rand()%size];
    fseek(randomFile, 0, SEEK_SET);
    fread(buffer, 10000, 1, randomFile);
    // system("echo masuk");
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

    sprintf(result, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
}

void zipAll() {
    pid_t child_id;
    int status;
    if(child_id = fork() == 0) {
        char *argv[] = {"zip", "-P", "satuduatiga", "-r", "not_safe_for_wibu.zip", "-q", "gacha_gacha", NULL};
        execv("/usr/bin/zip", argv);
    }
    while((wait(&status)) > 0);

    if(child_id = fork() == 0) {
        char *argv[] = {"rm", "-r", "gacha_database", NULL};
        execv("/usr/bin/rm", argv);
    }
    while((wait(&status)) > 0);

    // if(child_id = fork() == 0) {
        char *argv[] = {"rm", "-r", "gacha_gacha", NULL};
        execv("/usr/bin/rm", argv);
    // }
    // while((wait(&status)) > 0);
}
