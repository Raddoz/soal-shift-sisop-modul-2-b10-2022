#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>

char darat[15][100], air[15][100], bird[15][100], hewanAir[15][100];
int daratSize = 0, airSize = 0, birdSize = 0;

void createFolder(char *type)
{
    pid_t child_process;
    child_process = fork();
    int status;

    char path[] = "/home/fr3d3r1ck_ws31/modul2/";
    strcat(path, type);
    if (child_process == 0)
    {
        char *cmd[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void extractFile()
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"unzip", "-q", "animal.zip", "-d", "/home/fr3d3r1ck_ws31/modul2", NULL};
        execv("/bin/unzip", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void categorize()
{
    int x = 0, y = 0, z = 0;
    DIR *folder;
    struct dirent *de;
    char path[] = "/home/fr3d3r1ck_ws31/modul2/animal/";
    char birdPath[] = "/home/fr3d3r1ck_ws31/modul2/darat/";

    folder = opendir(path);
    while ((de = readdir(folder)) != NULL)
    {
        char *copy = (char *)malloc(sizeof(path) + sizeof(de->d_name));
        strcpy(copy, path);
        strcat(copy, de->d_name);

        char *copyBird = (char *)malloc(sizeof(birdPath) + sizeof(de->d_name));
        strcpy(copyBird, birdPath);
        strcat(copyBird, de->d_name);
        if (strstr(de->d_name, "darat"))
        {
            strcpy(darat[x++], copy);
            daratSize++;
        }
        else if (strstr(de->d_name, "air"))
        {
            strcpy(air[y], copy);
            strcpy(hewanAir[y++], de->d_name);
            airSize++;
        }
        if (strstr(de->d_name, "bird"))
        {
            strcpy(bird[z++], copyBird);
            birdSize++;
        }
    }
    closedir(folder);
}

void copyFile(int index, char *type)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        if (strcmp(type, "darat") == 0)
        {
            char *cmd[] = {"cp", darat[index], "/home/fr3d3r1ck_ws31/modul2/darat", NULL};
            execv("/bin/cp", cmd);
        }
        else
        {
            char *cmd[] = {"cp", air[index], "/home/fr3d3r1ck_ws31/modul2/air", NULL};
            execv("/bin/cp", cmd);
        }
    }

    while ((wait(&status)) > 0)
        ;
}

void removeBird(int index)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"rm", bird[index], NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void createList(int index)
{
    char permission[5];
    struct stat fs;
    int r;
    char path[] = "/home/fr3d3r1ck_ws31/modul2/air/";
    r = stat(path, &fs);
    if (r == -1)
    {
        fprintf(stderr, "File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(fs.st_uid);
    if (fs.st_mode & S_IRUSR)
        permission[0] = 'r';
    if (fs.st_mode & S_IWUSR)
        permission[1] = 'w';
    if (fs.st_mode & S_IXUSR)
        permission[2] = 'x';

    if (pw != 0)
    {
        FILE *wfile = fopen("/home/fr3d3r1ck_ws31/modul2/air/list.txt", "a");
        fprintf(wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]);
        fclose(wfile);
    }
}

int main()
{
    createFolder("darat");
    sleep(3);
    createFolder("air");
    extractFile();
    categorize();
    for (int i = 0; i < daratSize; i++)
        copyFile(i, "darat");
    for (int i = 0; i < airSize; i++)
        copyFile(i, "air");
    for (int i = 0; i < birdSize; i++)
        removeBird(i);
    for (int i = 0; i < airSize; i++)
        createList(i);
}