# soal-shift-sisop-modul-2-B10-2022

# Kelompok B10 Sistem Operasi | Praktikum Modul 2
    Elthan Ramanda B           <-->  5025201092
    Bimantara Tito Wahyudi     <-->  5025201227
    Frederick Wijayadi Susilo  <-->  5025201111

## Source Soal Shift 2
Soal dapat diakses <a href="https://docs.google.com/document/d/1fw7Yn4E3L54siTJQWk042cplltvMh-rHpIy3p97aXWA/edit?usp=sharing" target="_blank">disini</a>
## Penyelesaian Soal Shift 2

### Soal 1

Pada soal 1 kami diminta untuk membuat script yang bertujuan untuk melakukan gacha. Agar tidak dapat dipantau, proses ini harus berjalan di latar belakang sehingga proses ini merupakan daemon process

#### Source Code Daemon Process

![daemon process](./img/2_1_1.png)

#### Cara Pengerjaan

Tujuan: Membuat proses yang berjalan di latar belakang

1. Tentukan base directory proses
2. Melakukan langkah-langkah untuk membuat proses daemon sesuai yang tertera di modul dengan contoh directory `/home/yeray/Sisop/modul_2/soal_1`

#### Source Code Soal 1.a

![main](./img/2_1_2.png)
![downloadAsset](./img/2_1_3.png)
![storeFiles](./img/2_1_4.png)

#### Cara Pengerjaan

Tujuan: Mendownload file characters dan file weapons dari link yang tersedia kemudian mengekstrak kedua file tersebut. Setelah itu membuat folder `gacha_gacha` yang akan menjadi working directory

1. Melakukan `fork` untuk membuat proses baru yang bertujuan untuk melakukan download
2. Dalam fungsi `downloadAsset`, lakukan `fork` lagi untuk membuat folder `gacha_databse` sebagai directory untuk menyimpan hasil download menggunakan `execv`. Ganti directory menjadi folder `gacha_database` yang telah dibuat dengan `chdir` kemudian simpan link download dan nama filenya dalam array. Kemudian lakukan `fork` sebanyak 2x, satu child untuk mendownload file dari link dan yang satunya untuk melakukan unzip file yang sudah didownload. Lakukan langkah 4 untuk file characters dan weapons. Tidak lupa kembali ke directory awal setelah selesai
6. Setelah download selesai, lakukan `fork` untuk membuat folder `gacha_gacha` 
7. Buat sebuah string yang menyimpan `characters` kemudian buat sebuah array of files bernama `characters` yang nantinya akan digunakan untuk menyimpan seluruh file characters yang telah di download menggunakan fungsi `storeFiles`
8. Dalam fungsi `storeFiles`, pindah directory terlebih dahulu ke folder `gacha_database`. Setelah itu buka directory characters dengan `opendir` serta pindah ke directory tersebut. Lakukan perulangan untuk membuka dan menyimpan semua file ke dalam array of files tadi menggunakan `fopen` hingga mencapai end of directory. Tutup directory menggunakan `closedir` setelah selesai. Apabila directory tidak dapat dibuka , kirim pesan error dengan `perror` kemudian tutup proses dengan `exit` agar tidak menyebabkan error berkelanjutan nantinya. Setlah semua selesai, kembali ke directory awal
9. Lakukan langkah 7 dan 8 untuk file weapons

#### Source Code Soal 1.b dan 1.d

![gacha](./img/2_1_5.png)
![randomizer](./img/2_1_6.png)

#### Cara Pengerjaan

Tujuan: Ketika gacha, setiap jumlah gacha ganjil akan mengambil characters dan setiap genap akan mengambil weapons. Selain itu, akan dibuat sebuah file baru setiap gacha ke-10 dan folder baru setiap gacha ke-90.

1. Buat variabel untuk menyimpan waktu lokal menggunakan fungsi `time` dan `localtime`
2. Gunakan `for` untuk membuat perulangan di mana saat awal primo berjumlah 79000 akan berjalan selama primo masih lebih atau sama dengan 160 dan akan dikurangi 160 setiap kali gacha
3. Ketika `i`, jumlah gacha yg telah dilakukan, telah mencapai 90, tentukan apakah sisa primo masih bisa untuk 90 gacha kedepan. Apabila tidak mencukupi, hitung berapa banyak gacha yang bisa dilakukan dengan sisa primo yang ada. Hal ini dilakukan untuk menamai folder sesuai dengan format. Apabila bukan yang pertama kali, pindah directory terlebih dahulu ke `gacha_gacha`. Kemudian simpan nama folder sesuai format dalam string. Lakukan `fork` untuk membuat directory baru dengan nama sesuai string tadi. Setelah itu ganti directory ke folder baru yang telah dibuat
4. Apabila `i+1` genap maka gacha weapons, apabila ganjil, maka gacha characters. Proses gacha dilakukan dengan fungsi `randomizer`
5. Dalam fungsi `randomizer`, lakukan persiapan untuk parse file json. File yang diambil array yang menyimpan file adalah random menggunakan fungsi `rand`. Ambil 2 properti dari file json, yakni name dan rarity menggunakan fungsi `json_object_object_get_ex`. Setelah itu buat string yang akan menyimpan hasil name dan rarity yang telah diambil yang kemudian akan dikembalikan ke `main`
6. Gunakan hasil dari fungsi `randomizer` untuk melengkapi string yang akan di-`print` ke dalam file `.txt` hasil gacha.
7. Apabila semua proses gacha telah selesai, close file yang sedang terbuka, kembali ke base directory, dan matikan child proses gacha menggunakan `execv` yang menjalankan `echo` tanpa argument

#### Source Code Soal 1.c

![gacha](./img/2_1_7.png)

#### Cara Pengerjaan

Tujuan: Membuat file baru untuk menampung hasil gacha setiap gacha kelipatan 10

1. Simpan waktu saat ini menggunakan `time` dan `localtime`
2. Periksa apakah sisa primo saat ini dapat digunakan untuk 10 gacha lagi. Apabila tidak hitung berapa total gacha yang bisa dilakukan dengan sisa primo
3. Buat string dengan format sesuai dengan soal
4. Apabila bukan yang pertama kali, tutup terlebih dahulu file yang terbuka menggunakan `fclose`
5. Buat sekaligus buka file baru dengan nama sesuai string yang telah dibuat menggunakan fungsi `fopen`
6. Tahan proses selama 1 detik menggunakan `sleep`

#### Source Code Soal 1.e

![main](./img/2_1_8.png)
![zipAll](./img/2_1_9.png)

#### Cara Pengerjaan

Tujuan: Menjalankan proses terus menerus hingga waktu yang diinginkan terpenuhi. Selain itu, menyimpan semua hasil gacha pada folder `gacha_gacha` kedalam zip dan hapus semua folder

1. Gunakan `while(1)` agar proses daemon tetap berjalan hingga tujuannya terpenuhi
2. Simpan waktu saat ini menggunakan `time` dan `localtime`
3. Lakukan `fork` untuk mengecek apakah waktu saat ini sudah cocok dengan yang diinginkan
4. Apabila waktu menunjukkan 30 Maret 04:44, jalankan fungsi `gacha`
5. Apabila waktu menunjukkan 3 jam setelahnya, yakni 30 Maret 07:44, jalankan fungsi `zipAll`
6. Dalam fungsi `zipAll`, lakukan `fork` sebanyak 2x, 1 child process untuk melakukan zip pada folder `gacha_gacha` dengan nama `not_safe_for_wibu.zip` dan password `satuduatiga` menggunakan `execv` yang menjalankan command `zip`, dan child process lainnya untuk menghapus folder `gacha_database` menggunakan `execv`. Sedangkan parent process akan digunakan untuk menghapus folder `gacha_gacha` yang juga menggunakan `execv`
7. Apabila tidak ada yang terpenuhi, terminate child process menggunakan `execv` yang menjalankan `echo` tanpa argumen
8. Apabila waktu menunjukkan 30 Maret 07:44, setelah dipanggilnya fungsi `zipAll` parent process akan di-terminate menggunakan `exit`
9. Agar proses tidak terus menggunakan memori dan menghabiskan resources, semua proses dalam `while` akan dijalankan setiap 60 detik menggunakan fungsi `sleep`

#### Output Soal 1

1. Base Directory

![output](./img/2_1_10.png)

2. Directory gacha_database

![output](./img/2_1_11.png)

3. Isi folder characters setelah di-unzip

![output](./img/2_1_12.png)

4. Isi folder weapons setelah di-unzip

![output](./img/2_1_13.png)

5. Isi folder gacha_gacha

![output](./img/2_1_14.png)

6. Isi salah satu folder gacha

![output](./img/2_1_15.png)

7. File not_safe_for_wibu setelah di-zip

![output](./img/2_1_16.png)

7. Isi salah satu file gacha

![output](./img/2_1_17.png)

#### Kendala Soal 1

Tidak AC saat praktikum karena tadinya tidak tahu cara untuk debugging di daemon process

### Soal 2
Pada soal 2, Japrun mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi di dalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun meminta bantuan untuk menyelesaikan pekerjaannya.

**Note:**

**1. Setiap langkah pembuatan child process dilakukan dengan menginisialisasi variabel `child_process` dengan tipe data `pid_t` dengan memanggil function `fork()` yang merupakan function *system call* di C, kemudian jika `child_process` = 0, maka perintah akan dilakukan, lalu juga terdapat variabel `status` yang digunakan pada akhir function yaitu pada syntax `while ((wait(&status)) > 0)` yang menandai selesainya semua tugas pada suatu function.**

**2. Pada penyelesaian soal 2, soal 2d diselesaikan terlebih dahulu sebelum soal 2c.**

<details>
  <summary markdown="span">Tugas 2a</summary>

    Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting,
    maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan. 
</details>

- Source Code

![2a](./img/soal2a.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat folder drakor pada path "/home/fr3d3r1ck_ws31/shift2/drakor" dan mengextract file drakor.zip.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, pembuatan folder dan pengextractan file dilakukan dengan pembuatan function `createFolder()` dan function `extractFile()`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Pada function `createFolder()` berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"mkdir", "-p", path, NULL}` yang berarti membuat folder bahkan jika parent folder belum ada (akan dibuat juga parent foldernya) sesuai dengan path yang telah ditentukan.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Pada function `extractFile()` berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"unzip", "-q", "drakor.zip", "*.png", "-d", "/home/fr3d3r1ck_ws31/shift2/drakor", NULL}` yang berarti mengextract file `drakor.zip` dengan hanya mengambil file yang bertipe `png` dan diletakkan pada path `/home/fr3d3r1ck_ws31/shift2/drakor`, tetapi hasil yang diextract tidak memunculkan *verbose* pada terminal karena adanya argumen `-q`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2b</summary>

    Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip.
    Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip. 
    Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
</details>

- Source Code

![2b](./img/soal2b.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mengategorikan setiap film pada folder kategorinya.**

1. Membuat variabel `folder` dan `de`, kemudian membuka folder drakor sesuai path pada variabel `folder` dan membaca isi semua folder drakor dimana setiap file yang ada pada folder tersebut akan diassign di variabel `de` dengan mengabaikan file `.` dan `..`.
2. Menyimpan `de->d_name` atau nama file pada variabel string `edit`.
3. Memanggil function `delBack()` dengan parameter `edit` sebagai string yang ingin dihapus substringnya yang berada di belakang dan `.png` sebagai substring yang ingin dihapus. Cara kerja function ini adalah mengambil panjang substring yang ingin dihapus dan melakukan iterasi untuk menghapus substring pada suatu string dengan function yang ada pada library `string.h` yaitu `memmove` yang memanipulasi string dengan memindahkan indexnya sehingga substring dapat terhapus. Iterasi ini akan terus dilakukan jika masih ditemukan substring pada string tersebut.
4. Karena nama suatu file dapat mengandung 2 film berbeda yang dipisahkan dengan tanda `_`, maka tanda tersebut pada nama file akan diganti menjadi tanda `;` sehingga lebih mudah ketika memisahkan isi-isi pada nama file tersebut nantinya. Penggantian nama file dilakukan dengan iterasi sederhana dimana ketika ditemukan tanda `_`, maka akan digantikan oleh tanda `;`.
5. Membuat variabel string `filmName`, `year`, dan `category`, serta variabel `ind` yang diset valuenya adalah 1. Variabel `filmName` digunakan untuk menyimpan data berupa nama film dari sebuah file, variabel `year` digunakan untuk menyimpan data berupa tahun rilis film dari sebuah file, dan variabel `category` digunakan untuk menyimpan data berupa jenis film dari sebuah file. Sedangkan untuk variabel `ind` sendiri merupakan variabel yang digunakan sebagai penanda saat pemisahan konten file (nama film, tahun rilis film, atau jenis film) yang akan terus bertambah 1 setiap konten file yang ditemukan.
6. Pemisahan konten file dilakukan dengan function yang ada pada library `string.h` yaitu `strtok` dengan separator `;` hingga akhir string. Disinilah variabel-variabel tadi mulai bekerja. 
7. Saat `ind` dimodulo 3 menghasilkan nilai 1, maka konten file tersebut merupakan nama film sehingga konten file akan dimasukkan pada variabel `filmName`.
8. Saat `ind` dimodulo 3 menghasilkan nilai 2, maka konten file tersebut merupakan tahun rilis film sehingga konten file akan dimasukkan pada variabel `year`.
9. Saat `ind` dimodulo 3 menghasilkan nilai 0, maka konten file tersebut merupakan jenis film sehingga konten file akan dimasukkan pada variabel `category`. Kemudian, akan dibuat variabel string `dest` dengan isinya adalah path folder `drakor` dan akan ditambahkan variabel `category` tadi pada variabel `dest` sebagai path lanjutan dari folder `drakor`. Variabel `myFolder` dibuat dimana variabel tersebut akan menampung path `dest`. Jika folder yang sesuai dengan path `dest` tersebut belum ada, maka akan dibuat folder dengan path tersebut. 
10. Lakukan iterasi pada setiap file hingga selesai sehingga setiap film terkategorikan sesuai jenisnya.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2c</summary>

    Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
    Contoh: “/drakor/romance/start-up.png”.
</details>

- Source Code

![2c.1](./img/soal2c.1.png)

![2c.2](./img/soal2c.2.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Memindahkan poster ke folder dengan kategori yang sesuai dan merename nama poster.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terdapat function `renameFile()` dengan parameter `src` yang merupakan nama asli poster dan `path` yang merupakan nama pengganti poster. Pada function ini berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"mv", src, path, NULL}` yang berarti merename poster dengan manipulasi menggunakan `mv` yang memindahkan file sesuai dengan path yang telah ditentukan.

1. Pada saat pengerjaan soal 2b, saat membuat folder kategori, dibuatlah variabel string `from` yang akan dipassing sebagai variabel `src` pada function `renameFile()` nantinya.
2. Variabel `from` yang mula-mulanya memiliki value berupa path folder `drakor` kemudian ditambahkan variabel `category` serta nama file yang ingin direname. Sedangkan, variabel `dest` yang sudah memiliki path hingga folder kategori film, akan dimodifikasi dengan ditambahkan nama film pengganti dengan format `namaFilm.png`.
3. Kemudian, akan dipanggil function `renameFile()` dengan passing variabel `from` sebagai variabel `src` dan variabel `dest` sebagai variabel `path` pada function sehingga nama file akan terganti dengan nama file yang baru.

**Note: Pemindahan poster ke folder kategori akan sekaligus dilakukan di soal 2d.**

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2d</summary>

    Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai.
    Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.
    (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto).
</details>

- Source Code

![2d.1](./img/soal2d.1.png)

![2d.2](./img/soal2d.2.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Jika terdapat nama file berisi 2 film sekaligus, maka masing-masing film akan dipindahkan ke kategori yang sesuai.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terdapat function `copyFile()` dengan parameter `src` yang merupakan path asal poster dan `path` yang merupakan path yang ingin dituju. Pada function ini berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"cp", src, path, NULL}` yang berarti menyalin poster dari path asal menuju ke path yang telah ditentukan.

1. Pada saat pengerjaan soal 2b, saat membuat folder kategori, dibuatlah variabel string `src` yang akan dipassing sebagai variabel `src` pada function `copyFile()` nantinya.
2. Variabel `src` yang mula-mulanya memiliki value berupa path folder `drakor` kemudian ditambahkan nama file yang ingin dipindahkan. Sedangkan, variabel `dest` akan dipakai dengan sudah memiliki path hingga folder kategori film.
3. Kemudian, akan dipanggil function `copyFile()` dengan passing variabel `src` sebagai variabel `src` dan variabel `dest` sebagai variabel `path` pada function sehingga file akan berpindah menuju ke path kategorinya masing-masing.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2e</summary>

    Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).
    Format harus sesuai contoh dibawah ini.
    kategori : romance
    
    nama : start-up
    rilis  : tahun 2020
    
    nama : twenty-one-twenty-five
    rilis  : tahun 2022
</details>

- Source Code

![2e.1](./img/soal2e.1.png)

![2e.2](./img/soal2e.2.png)

![2e.3](./img/soal2e.3.png)

![2e.4](./img/soal2e.4.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat file "data.txt" di setiap folder kategori yang berisi nama dan tahun rilis semua drama korea.**

1. Menginisialisasi array 3D `input` bertipe char yang dimana dimensi pertama menunjukkan indeks kategori/jenis film, dimensi kedua menujukkan indeks drama korea pada suatu folder kategori, dan dimensi ketiga merupaka panjang string dari nama file. Selain itu, juga diinisialisasi array 2D `kategori` bertipe char yang dimana dimensi pertama menunjukkan indeks kategori/jenis film dan dimensi kedua merupakan panjang string dari nama file. Inisialisasi variabel `jenis` dengan nilai 0 sebagai penampung jumlah jenis film yang ada dan variabel `last[10]` yang berguna untuk menampung jumlah film pada kategori tertentu terakhir kali saat berada pada suatu folder kategori.
2. Pada saat pengerjaan soal 2b, saat membuat folder kategori, dimasukkan juga nama setiap kategori pada array 2D `kategori` dengan indeks variabel `jenis` yang bertambah 1 sehingga `jenis` hanya akan bertambah ketika membuat folder kategori baru. Contoh: kategori yang pertama kali ditemukan adalah romance, sehingga `kategori[0]` akan berisi romance, dan `jenis` akan menjadi 1. Dilakukan juga iterasi sebanyak jumlah jenis yang ada sekarang dimana di setiap iterasi akan dicek apakah kategori yang didapat sekarang sudah terdapat pada array `kategori` atau belum. Jika sudah, maka akan dibuat variabel string `tmp` yang akan menampung format string `year_filmName` untuk dimasukkan pada array 3D `input` dengan indeks dimensi pertama yaitu jenis film sekarang dan indeks dimensi kedua berupa variabel `last[i]` yaitu jumlah film terakhir pada kategori itu dengan `last[i]` ini akan bertambah 1 setiap film baru masuk pada `input`.
3. Melakukan langkah 2 terus-menerus hingga film terakhir.
4. Membuat function `sort()` yang berguna untuk mengurutkan data film di setiap kategorinya dengan membuat variabel string `tmp` untuk menampung string sementara. Iterasi dilakukan sebanyak jumlah kategori film yang ada dengan setiap iterasinya akan dilakukan `bubble sort` hingga semua film pada setiap folder kategorinya terurut berdasarkan tahun rilis (pengurutan string secara leksikografis).
5. Membuat function `inputFile()` yang berguna untuk mengirimkan semua isi array 3D `input` pada file "data.txt" sesuai folder kategorinya. Dilakukan iterasi sebanyak jumlah kategori film yang ada dimana di setiap iterasinya akan dibuat variabel string `dest` yang akan diisi path folder `drakor` kemudian ditambahkan nama kategori film pada pathnya. Lalu, akan dilakukan iterasi sebanyak `last[i]` yaitu jumlah film terakhir pada kategori tersebut dan di setiap iterasi akan dibuat variabel string `edit` dengan isinya adalah `input[i][j]` yaitu string nama film dan tahun rilis yang telah diformat menjadi `year_filmName`, kemudian  
dibuat variabel string `filmName` dan `year`, serta variabel `ind` yang diset valuenya adalah 1. Variabel `filmName` digunakan untuk menyimpan data berupa nama film dari variabel `edit` dan variabel `year` digunakan untuk menyimpan data berupa tahun rilis film dari variabel `edit`. Sedangkan untuk variabel `ind` sendiri merupakan variabel yang digunakan sebagai penanda saat pemisahan konten file (nama film dan tahun rilis film) yang akan terus bertambah 1 setiap konten file yang ditemukan. Pemisahan konten file dilakukan dengan function yang ada pada library `string.h` yaitu `strtok` dengan separator `_` hingga akhir string. Disinilah variabel-variabel tadi mulai bekerja. Saat `ind` dimodulo 2 menghasilkan nilai 0, maka konten `edit` tersebut merupakan nama film sehingga akan dimasukkan pada variabel `filmName`, sedangkan saat `ind` dimodulo 2 menghasilkan nilai 1, maka konten `edit` tersebut merupakan tahun rilis film sehingga akan dimasukkan pada variabel `year`. Selanjutnya, akan dibuat variabel string `loc` yang akan diisi variabel `dest` kemudian ditambahkan string `/data.txt` yang merupakan path lanjutan. Setelah itu akan dicek, apabila file "data.txt" pada path yang ingin diakses tersebut ditemukan, maka penambahan konten pada file menggunakan `fopen()` bertipe `a` atau `append` dengan penambahan kontennya yaitu nama film dan tahun rilis film, sedangkan apabila file "data.txt" pada path yang ingin diakses tersebut tidak ditemukan, maka penambahan konten pada file menggunakan `fopen()` bertipe `w` atau `write` dengan penambahan kontennya yaitu kategori film, nama film, dan tahun rilis film. Konten pada file akan terurut berdasarkan tahun rilis karena sudah melalui proses `sorting`.

- Kendala

1. Menemukan cara `sorting` berdasarkan tahun rilis film yang efisien.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi: Memasukkan nama film dan tahun rilis film pada sebuah string dengan format tertentu dan dilakukan `bubble sort` secara leksikografis.

#### Output Soal 2

1. Proses dan hasil compile soal 2.

![out1](./img/outputCompile.png)

2. Output telah diextractnya file `drakor.zip` dan dibuatnya folder `drakor` serta berbagai folder kategori film.

![out2](./img/outputFolderDrakorKategori.png)

3. Output pada folder `action` dan `comedy`.

![out3](./img/outputActionComedy.png)

4. Output pada folder `fantasy` dan `horror`.

![out4](./img/outputFantasyHorror.png)

5. Output pada folder `romance` dan `thriller`.

![out5](./img/outputRomanceThriller.png)

6. Output pada folder `school`.

![out6](./img/outputSchool.png)

### Soal 3

Pada soal 3, Conan disini sebagai detektif memiliki sebuah tugas mulia untuk mengklasifikasikan beberapa hewan yang dilaporkan hilang pada suatu kebun binatang. Tugas dia yaitu membagi hewan-hewan tersebut kedalam folder air dan darat, akan tetapi karena kebun binatang tersebut sudah memiliki cukup burung, maka Conan diminta untuk tidak memasukkan daftar burung ke dalam pembagian tersebut dan terakhir membuat sebuah list yang nanti berada pada folder air

**Note:**

**1. Setiap langkah pembuatan child process dilakukan dengan menginisialisasi variabel `child_process` dengan tipe data `pid_t` dengan memanggil function `fork()` yang merupakan function *system call* di C, kemudian jika `child_process` = 0, maka perintah akan dilakukan, lalu juga terdapat variabel `status` yang digunakan pada akhir function yaitu pada syntax `while ((wait(&status)) > 0)` yang menandai selesainya semua tugas pada suatu function.**

<details>
  <summary markdown="span">Tugas 3a</summary>

    Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.  
</details>

- Source Code

![3ae](./img/3ae.png)
![3aa](./img/3aa.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat folder "darat" dan "air" pada path "/home/fr3d3r1ck_ws31/modul2/".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, pembuatan folder dilakukan dengan pembuatan function `createFolder()` akan tetapi, karena pada soal diminta ada jeda 3 detik saat pembuatan folder "darat" dan "air" maka digunakan command `sleep(3)` yang akan membuat program membuat folder dengan jeda 3 detik sesudah membuat folder pertama.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada function `createFolder()` berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"mkdir", "-p", path, NULL}` yang berarti membuat folder bahkan jika parent folder belum ada (akan dibuat juga parent foldernya) sesuai dengan path yang telah ditentukan.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3b</summary>

    Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
</details>

- Source Code

![3bb](./img/3bb.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mengekstrak file yang ada pada animal.zip ke directory "/home/fr3d3r1ck_ws31/modul2/".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Function `extractFile()` berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command `execv` dengan argumen `{"unzip", "-q", "animal.zip", "-d", "/home/fr3d3r1ck_ws31/modul2", NULL}` yang berarti mengextract file dari `animal.zip` yang kemudian diletakkan pada path `/home/fr3d3r1ck_ws31/modul2/animal`, tetapi hasil yang diextract tidak memunculkan *verbose* pada terminal karena adanya argumen `-q`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3c</summary>

    Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
</details>

- Source Code

![3cd](./img/3cd.png)

![3cc](./img/3cc.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Memasukkan file dari hasil ekstrak ke folder darat dan air pada directory "/home/fr3d3r1ck_ws31/modul2/".**

1. Pada function categorize() pertama dibuat variabel `x`,` y`, dan z untuk digunakan sebagai indeks array yang nantinya digunakan pada array `darat`, `air`, `hewanAir`, dan `bird`.
2. Selanjutnya akan membuka path "/home/fr3d3r1ck_ws31/modul2/animal/" dan membaca semua isi filenya, kemudian menggunakan malloc untuk menge-set size dari string copy dan `copybird`. Untuk string copy akan berisi file pada folder animal dan string copyBird akan berisi file pada folder `darat`.
3. Selanjutnya program akan mengecek apakah terdapat substring `darat`, `air`, dan bird pada setiap nama file, lalu jika ditemukan, path file akan dimasukkan ke masing masing array yang sesuai dengan indeks akan bertambah 1, serta akan ada variabel yang bertambah sebagai jumlah hewan yang bertipe tertentu. Array bukan berisi nama hewan, tetapi akan berisi path yang merujuk ke nama hewan tersebut.
4. Array hewanAir nanti akan digunakan pada function `createList()`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pada function `copyFile()`, file yang sudah dikategorikan pada function `categorize()` selanjutnya akan dipindahkan ke masing masing directory yaitu `"/home/fr3d3r1ck_ws31/modul2/darat"` dan `"/home/fr3d3r1ck_ws31/modul2/air"` dengan memperhatikan index `darat` dan `air` dan menggunakan argumen `cp` untuk meng-copynya.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3d</summary>

    Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
</details>

- Source Code

![3dd](./img/3dd.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Menghapus hewan burung pada folder darat yang ditandai dengan _bird pada nama filenya.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Function `removeBird()` dengan menggunakan fungsi `remove` `rm` ditujukan untuk menghilangkan file yang ditandai dengan `bird` dengan membaca index dari setiap file yang ada, sehingga jika terdapat index `bird` maka file tersebut akan terhapus.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3e</summary>

    Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png
</details>

- Source Code

![3e](./img/3e.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat file list.txt yang berada pada "/home/fr3d3r1ck_ws31/modul2/air".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Function `createList()` disini merupakan sebuah function yang ditujukan untuk membuat `list.txt` dengan format **UID_[UID file permission]_Nama File.[jpg/png]**. List ini dibuat pada path `"/home/fr3d3r1ck_ws31/modul2/air/"` dimana saat file ini dibuat, akan menunjukkan juga file permission dan menampilkan UID user yang kemudian di printkan di dalam `list.txt` menggunakan perintah `wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `getpwuid` digunakan untuk mendapatkan informasi user pemakai dan untuk permission file yang ada pada directory air apakah read, write, dan execute yang kemudian informasi tersebut disimpan pada variabel permission didapat dari status tertentu, kemudian format yang diinginkan di print sesuai dengan format diatas.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

#### Output Soal 3

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Pada output disini menggunakan user elrado, berbeda dengan yang di gitlab yaitu fr3d3r1ck_ws31.**

1. Proses dan hasil compile soal 3.

![Modul2_gcc](./img/Modul2_gcc.png)

![Modul2_exe](./img/Modul2_exe.png)

2. Membuat folder `darat` dan `air` pada directory modul2.

![Modul2_dir](./img/Modul2_dir.png)

3. Output telah diextractnya file `animal.zip` dan dibuatnya folder `animal` pada directory modul2.

![Modul2_dir](./img/Modul2_dir.png)

![Modul2_animal](./img/Modul2_animal.png)

4. Membagi file ke dalam masing - masing folder  darat dan air sesuai dengan indexnya dari folder `animal` pada directory modul2, dan tidak lupa tidak mengikutkan file yang memiliki index `bird` sehingga tidak masuk ke folder `darat` maupun `air`.

![Modul2_darat](./img/Modul2_darat.png)

![Modul2_air](./img/Modul2_air.png)

5. Membuat file `list.txt` yang berada di dalam folder `air`.

![Modul2_air](./img/Modul2_air.png)

![Modul2_list](./img/Modul2_list.png)

## Referensi Soal Shift 2

1. https://stackoverflow.com/questions/11736060/how-to-read-all-files-in-a-folder-using-c
2. https://stackoverflow.com/questions/12510874/how-can-i-check-if-a-directory-exists
3. https://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c
